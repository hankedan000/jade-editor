function jadeEditor(container){
  console.log('initlizing jade-editor');
  createToolbar(container);
  createTextField(container);
  createToolbarCallbacks(container);
  console.log('done');
}

function createToolbar(container){
  // make a tool bar object
  var toolbar = $('<ul>');
  toolbar.addClass('nav');
  toolbar.addClass('nav-pills');

  // make a font button
  var fontButton = $('<li>');
  fontButton.addClass('active');
  fontButton.html('<a href="#" id="editor-control-font"><i class="fa fa-font" aria-hidden="true"></i></a>');
  toolbar.append(fontButton);

  // make a bold font button
  var boldButton = $('<li>');
  boldButton.addClass('active');
  boldButton.html('<a href="#" id="editor-control-bold"><i class="fa fa-bold" aria-hidden="true"></i></i></a>');
  toolbar.append(boldButton);

  // make an italic font button
  var italicButton = $('<li>');
  italicButton.addClass('active');
  italicButton.html('<a href="#" id="editor-control-italic"><i class="fa fa-italic" aria-hidden="true"></i></i></a>');
  toolbar.append(italicButton);

  // make an underlined font button
  var underlineButton = $('<li>');
  underlineButton.addClass('active');
  underlineButton.html('<a href="#" id="editor-control-underline"><i class="fa fa-underline" aria-hidden="true"></i></i></a>');
  toolbar.append(underlineButton);

  // make an image button
  var imageButton = $('<li>');
  imageButton.addClass('active');
  imageButton.html('<a href="#" id="editor-control-img"><i class="fa fa-picture-o" aria-hidden="true"></i></i></a>');
  imageButton.attr('data-toggle','popover');
  imageButton.attr('data-placement','bottom');
  var imgPopoverContent = $('<div>');
  var imgUrlGroup = $('<div class="input-group">');
  imgUrlGroup.append($('<input type="text" class="form-control" placeholder="img url">'));
  var imgGoButton = $('<button class="btn btn-secondary jade-editor-img-button">Go!</button>');
  imgUrlGroup.append($('<span class="input-group-btn"></span>').append(imgGoButton));
  imgPopoverContent.append(imgUrlGroup);
  imageButton.attr('data-content',imgPopoverContent.html());
  toolbar.append(imageButton);

  // make a hyperlink button
  var linkButton = $('<li>');
  linkButton.addClass('active');
  linkButton.html('<a href="#" id="editor-control-link"><i class="fa fa-link" aria-hidden="true"></i></i></a>');
  linkButton.attr('data-toggle','popover');
  linkButton.attr('data-placement','bottom');
  var linkPopoverContent = $('<div>');
  var linkUrlGroup = $('<div class="input-group">');
  linkUrlGroup.append($('<input type="text" class="form-control" placeholder="url">'));
  var linkGoButton = $('<button class="btn btn-secondary jade-editor-link-button">Go!</button>');
  linkUrlGroup.append($('<span class="input-group-btn"></span>').append(linkGoButton));
  linkPopoverContent.append(linkUrlGroup);
  linkButton.attr('data-content',linkPopoverContent.html());
  toolbar.append(linkButton);

  // make a table button
  var tableButton = $('<li>');
  tableButton.addClass('active');
  tableButton.html('<a href="#" id="editor-control-table"><i class="fa fa-table" aria-hidden="true"></i></i></a>');
  toolbar.append(tableButton);

  container.append(toolbar);
  $(document).on(
    'click',
    '.jade-editor-img-button',
    {
      editor:container
    },
    function(event){
      var imgUrl = $(this).closest('.input-group').find('input').val();
      var textField = $(event.data.editor).find('textarea');
      var tri = getTriSel(textField);
      textField.val(
        tri[0] + 'img(src="' + imgUrl + '")' + tri[2]
      );

      // close the popover
      $(event.data.editor).find('#editor-control-img').parent().popover('hide');
    }
  )

  $(document).on(
    'click',
    '.jade-editor-link-button',
    {
      editor:container
    },
    function(event){
      var linkUrl = $(this).closest('.input-group').find('input').val();
      console.log(linkUrl);
      var textField = $(event.data.editor).find('textarea');
      var tri = getTriSel(textField);
      textField.val(
        tri[0] + '#[a(href="' + linkUrl + '") ' + tri[1] + ']' + tri[2]
      );

      // close the popover
      $(event.data.editor).find('#editor-control-link').parent().popover('hide');
    }
  )
}

function createTextField(container){
  var ws = $('<textarea>');
  ws.attr('id','workspace');
  ws.css('width', '100%');
  ws.css('height', '400px');
  ws.css('font-size', '14px');

  container.append(ws);
}

function createToolbarCallbacks(container){
  container.find('#editor-control-font').click(function(event){
    alert('changing font');
  })
  container.find('#editor-control-bold').click(function(event){
    applyBIU(container.find('#workspace'),'b');
  })
  container.find('#editor-control-italic').click(function(event){
    applyBIU(container.find('#workspace'),'i');
  })
  container.find('#editor-control-underline').click(function(event){
    applyBIU(container.find('#workspace'),'u');
  })
  container.find('#editor-control-img').click(function(event){
    // alert('inserting an image');
    // applyBIU(container.find('#workspace'),'u');
  })
  container.find('#editor-control-link').click(function(event){
    // alert('inserting a link');
    // applyBIU(container.find('#workspace'),'u');
  })
  container.find('#editor-control-table').click(function(event){
    alert('inserting table');
    // applyBIU(container.find('#workspace'),'u');
  })
}

function applyBIU(field, BIU) {
  var tri = getTriSel(field);
  field.val(
    tri[0] +
    '#[' + BIU + ' ' + tri[1] + ']' +
    tri[2]
  );
}

/* Returns 3 strings, the part before the selection, the part after the selection and the selected part */
function getTriSel(field){
  var u     = field.val();
  var start = field.get(0).selectionStart;
  var end   = field.get(0).selectionEnd;

  return [u.substring(0, start), u.substring(start, end), u.substring(end)];
}
