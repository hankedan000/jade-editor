# jade-editor
## Overview
This is a web based editor for writing jade templated pages. I made it for my personal site to create and edit jade formated blog posts. It uses bootstrap for the styling and javascript/jquery for the logic.
## Use
Initialize it by passing an empty div element to the constructor.
